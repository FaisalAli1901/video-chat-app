import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';

Template.hello.onCreated(function helloOnCreated() {
    // Create a Peer instance
	window.peer = new Peer({
        key: '3oyncgfqskhxs9k9',
        debug: 3,
        config: { 'iceServers': [
            { url: 'stun:stun.l.google.com:19302' },
            { url: 'stun:stun1.l.google.com:19302' },
        ]}
    });

    // Handle event: upon opening our connection to the PeerJS server
    peer.on("open", () => {
        $(myPeerId).text(peer.id)
    });

    // Handle event: remote peer receives a call
    peer.on("call", (incomingCall) => {
        window.currentCall = incomingCall;
        incomingCall.answer(window.localStream);

        incomingCall.on("stream", (remoteStream) => {
            window.remoteStream = remoteStream;
            var video = document.getElementById("theirVideo")

            video.src = URL.createObjectURL(remoteStream)
        })
    })

    navigator.getUserMedia = (navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia ||
        navigator.msGetUserMedia);

    // get audio/video
    navigator.getUserMedia({ audio: true, video: true}, (stream) => {
        //display video
        var video = document.getElementById("myVideo")

        video.src = URL.createObjectURL(stream)
        window.localStream = stream;
    },(error) => {
        console.log(error);
    })  
});

Template.hello.helpers({
	
});

Template.hello.events({
	"click #makeCall": () => {
        let outGoingCall = peer.call($("#remotePeerId").val(), window.localStream);

        window.currentCall = outGoingCall;

        outGoingCall.on("stream", (remoteStream) => {
            window.remoteStream = remoteStream;
            let video = document.getElementById('theirVideo');

            video.src = URL.createObjectURL(remoteStream);
        })
    },
    "click #endCall": () => {
        window.currentCall.close()
    }
});
